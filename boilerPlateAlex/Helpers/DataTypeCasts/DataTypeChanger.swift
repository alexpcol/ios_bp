//
//  JSONToDictionary.swift
//  boilerPlateAlex
//
//  Created by Mario on 9/18/18.
//  Copyright © 2018 Mario. All rights reserved.
// In this controller I can make the correspondent casts if i have customs models to make arrays, get the value of the keys of stuff

import UIKit

class DataTypeChanger: NSObject
{
    private override init() {}
    static func JSONDataToDiccionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    static func DictionaryToJSONData(jsonObject: AnyObject) throws -> String?
    {
        let data: NSData? = try? JSONSerialization.data(withJSONObject: jsonObject, options: JSONSerialization.WritingOptions.prettyPrinted) as NSData
        
        var jsonStr: String?
        if data != nil {
            jsonStr = String(data: data! as Data, encoding: String.Encoding.utf8)
        }
        
        return jsonStr
    }
}
