//
//  DeviceHelpers.swift
//  boilerPlateAlex
//
//  Created by Mario on 10/8/18.
//  Copyright © 2018 Mario. All rights reserved.
//

import UIKit
import AVKit
import Foundation
import SystemConfiguration

class DeviceHelpers: NSObject {
    
    private override init() {}
    
    //MARK: - Network
    static func hasInternet()->Bool
    {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        return (isReachable && !needsConnection)
    }
    //MARK: - Camera
    static func prepareCamera(view: UIViewController) -> Bool
    {
        if self.deviceHasCamera()
        {
            switch self.getCameraAuthStatus()
            {
            case .authorized:
                return true
                
            case .denied:
                AlertsPresenter.showAlertWithAction(alertTitle: NSLocalizedString(MessagesTitle.accessToCameraTitle.rawValue, comment: "nil"), alertMessage: NSLocalizedString(MessagesText.accessToCameraMessage.rawValue, comment: "nil"), actionTitle: NSLocalizedString(ButtonsTitle.openSettings.rawValue, comment: "nil"), actionStyle: .cancel, inView: view) {
                    UIApplication.shared.open(URL(string:UIApplication.openSettingsURLString)!, options: [:], completionHandler: nil)
                }
                return false
                
            case .notDetermined:
                self.notDetermined(view: view)
                return false
                
            default:
                self.notDetermined(view: view)
                return false
            }
        }
        else {
            AlertsPresenter.showOKAlert(title: NSLocalizedString(MessagesTitle.camera.rawValue, comment: "nil"), message: NSLocalizedString(MessagesText.noCameraInDevice.rawValue, comment: "nil"), view: view)
            return false
        }
    }
    //MARK: - Camera Helpers
    private static func notDetermined(view: UIViewController)
    {
        AlertsPresenter.showAlertWithAction(alertTitle: NSLocalizedString(MessagesTitle.accessToCameraTitle.rawValue, comment: "nil"), alertMessage: NSLocalizedString(MessagesText.accessToCameraMessage.rawValue, comment: "nil"), actionTitle: NSLocalizedString(ButtonsTitle.allow.rawValue, comment: "nil"), actionStyle: .default, inView: view) {
            AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { (granted) in
                if granted
                {
                    DispatchQueue.main.async {
                        NotificationCenter.default.post(name: .showCameraOptionsAgain, object: nil, userInfo: [NotificationCenterUserInfoKeys.showCameraAgain.rawValue : true] )
                    }
                }
                
            })
        }
    }
    private static func deviceHasCamera() -> Bool
    {
        return UIImagePickerController.isSourceTypeAvailable(.camera)
    }
    
    private static func getCameraAuthStatus() -> AVAuthorizationStatus
    {
        return AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
    }
    
    //MARK: - Haptic Methods
    static func notificationFeedback(for type: UINotificationFeedbackGenerator.FeedbackType)
    {
        let generator = UINotificationFeedbackGenerator()
        generator.notificationOccurred(type)
    }
    static func hapticFeedback()
    {
        let generator = UIImpactFeedbackGenerator(style: UIImpactFeedbackGenerator.FeedbackStyle.medium)
        generator.impactOccurred()
    }
    
}
