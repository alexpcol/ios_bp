//
//  LocationHelper.swift
//  boilerPlateAlex
//
//  Created by Mario on 10/17/18.
//  Copyright © 2018 Mario. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit
class LocationHelper: NSObject {
    private override init() {}
    
    private let locationManager = CLLocationManager()
    var newLocation: CLLocation?
    static let shared = LocationHelper()
    private weak var view: UIViewController!
    
    func authorize(in view: UIViewController)
    {
        self.view = view
        setUpLocationManager()
        switch getLocationAuthStatus() {
        case .authorizedAlways?, .authorizedWhenInUse?:
            locationManager.startUpdatingLocation()
            break
        case .denied?, .restricted?:
            AlertsPresenter.showAlertWithAction(alertTitle: NSLocalizedString(MessagesTitle.accessToLocationTitle.rawValue, comment: "nil"), alertMessage: NSLocalizedString(MessagesText.accessToLocationMessage.rawValue, comment: "nil"), actionTitle: NSLocalizedString(ButtonsTitle.openSettings.rawValue, comment: "nil"), actionStyle: .default, inView: view) {
                UIApplication.shared.open(URL(string:UIApplication.openSettingsURLString)!, options: [:], completionHandler: nil)
            }
            break
        case .notDetermined?:
            locationManager.requestAlwaysAuthorization()
            break
        default:
            AlertsPresenter.showOKAlert(title: "Error", message: "Something went wrong with location", view: view) // This hardcoded string just for the lols
            break
        }
    }
    
    private func setUpLocationManager()
    {
        locationManager.delegate = self
        //locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
    }
    
    private func getLocationAuthStatus() ->CLAuthorizationStatus?
    {
        if CLLocationManager.locationServicesEnabled()
        {
            switch CLLocationManager.authorizationStatus()
            {
                case .authorizedAlways:
                    return .authorizedAlways
                case .authorizedWhenInUse:
                    return .authorizedWhenInUse
                case .denied:
                    return .denied
                case .notDetermined:
                    return .notDetermined
                case .restricted:
                    return .restricted
            }
        }
        else {return nil}
    }

    func getLocationFromMapTouch(sender: UIGestureRecognizer, in map: MKMapView) -> CLLocationCoordinate2D
    {
        let touchPoint = sender.location(in: map)
        return map.convert(touchPoint, toCoordinateFrom: map)
    }
}

extension LocationHelper: CLLocationManagerDelegate
{
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let currentNewLocation = newLocation
        {
            guard let currentLocation = locations.first else { return }
            let distanceInMeters = currentNewLocation.distance(from: currentLocation)
            if distanceInMeters > 10
            {
                newLocation = currentLocation
                NotificationCenter.default.post(name: .locationUpdated, object: nil, userInfo: [NotificationCenterUserInfoKeys.locationUpdated.rawValue: currentLocation])
            }
        }
        else
        {
            guard let currentLocation = locations.first else { return }
            newLocation = currentLocation
            NotificationCenter.default.post(name: .locationUpdated, object: nil, userInfo: [NotificationCenterUserInfoKeys.locationUpdated.rawValue: currentLocation])
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .authorizedAlways, .authorizedWhenInUse:
            locationManager.startUpdatingLocation()
            break
        case .denied, .restricted:
            AlertsPresenter.showAlertWithAction(alertTitle: NSLocalizedString(MessagesTitle.accessToLocationTitle.rawValue, comment: "nil"), alertMessage: NSLocalizedString(MessagesText.accessToLocationMessage.rawValue, comment: "nil"), actionTitle: NSLocalizedString(ButtonsTitle.openSettings.rawValue, comment: "nil"), actionStyle: .default, inView: self.view) {
                UIApplication.shared.open(URL(string:UIApplication.openSettingsURLString)!, options: [:], completionHandler: nil)
            }
            break
        case .notDetermined:
            break
        }
    }
}
