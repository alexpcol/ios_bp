//
//  MapsHelper.swift
//  boilerPlateAlex
//
//  Created by Mario on 10/17/18.
//  Copyright © 2018 Mario. All rights reserved.
//

import UIKit
import MapKit
class MapsHelper: NSObject {

    private override init() {}
    
    static func createRegionForMap(latitude: CLLocationDegrees,
                                   longitude: CLLocationDegrees,
                                   latDelta: CLLocationDegrees,
                                   lonDelta: CLLocationDegrees) -> MKCoordinateRegion
    {
        let span: MKCoordinateSpan = MKCoordinateSpan(latitudeDelta: latDelta, longitudeDelta: lonDelta)
        let location: CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        
        return MKCoordinateRegion(center: location, span: span)
    }
    
    static func addPin(latitude:CLLocationDegrees, longitude: CLLocationDegrees ,with title: String, and subtitle: String,in map: MKMapView)
    {
        let pinLocation: CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        let pin = MKPointAnnotation()
        pin.coordinate = pinLocation
        pin.title = title
        pin.subtitle = subtitle
        map.addAnnotation(pin)
    }
}
