//
//  RequestServicesName.swift
//  boilerPlateAlex
//
//  Created by Mario on 9/18/18.
//  Copyright © 2018 Mario. All rights reserved.
//

import UIKit

class APIServicesNames: NSObject
{
    //URL Prod
    static let URLService : String = "http://api.chila.com"
    
    weak var delegate : ResponseServicesProtocol?
    
    override init()
    {
        super.init()
    }
    
    init(delegate: ResponseServicesProtocol)
    {
        self.delegate = delegate;
        super.init()
    }
    
    //delegate Methods (should be in the delegate ViewController)
    func onSucces(Result : String, name : ServicesNames)
    {
        delegate?.onSucces(Result: Result, name: name);
    }
    
    func onError(Error : String, name : ServicesNames)
    {
        delegate?.onError(Error: Error, name: name);
    }
}
