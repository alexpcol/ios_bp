//
//  BiometricsBase.swift
//  boilerPlateAlex
//
//  Created by Mario on 10/19/18.
//  Copyright © 2018 Mario. All rights reserved.
//

import UIKit
import LocalAuthentication

enum BiometricType {
    case none
    case touchID
    case faceID
}
class BiometricsBase: NSObject {
    let context  = LAContext()
    var loginReason = "Autenticación con Touch ID/Face ID"
    var biometricsMethod: String = {
        let context = LAContext()
        if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil)
        {
            if #available(iOS 11.0, *)
            {
                switch context.biometryType
                {
                case .none:
                    return "none"
                case .touchID:
                    return "Touch ID"
                case .faceID:
                    return "Face ID"
                }
            }
            else { return "Touch ID" }
        }
        else {return "none"}
        
    }()
    
}
