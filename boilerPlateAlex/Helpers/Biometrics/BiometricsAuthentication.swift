//
//  BiometricsAuthentication.swift
//  boilerPlateAlex
//
//  Created by Mario on 10/8/18.
//  Copyright © 2018 Mario. All rights reserved.
//

import UIKit
import LocalAuthentication

class BiometricIDAuth: BiometricsBase {
    
    private override init() {}
    
    static let shared = BiometricIDAuth()
    
    
    func canEvaluatePolicy() -> Bool {
        return context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil)
    }
    
    func authenticateUser(completion: @escaping (String?) -> Void) {
        guard canEvaluatePolicy() else {
            completion(ErrorMessages.biometryNotAvailable.rawValue.replacingOccurrences(of: "BIOMETRIC", with: biometricsMethod))
            return
        }
        context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: loginReason) { (success, evaluateError) in
            if success {
                DispatchQueue.main.async {
                    // User authenticated successfully, take appropriate action
                    completion(nil)
                }
            } else {
                let message: String
                
                if #available(iOS 11.0, *)
                {
                    switch evaluateError
                    {
                    case LAError.authenticationFailed?:
                        message = ErrorMessages.authenticationFailed.rawValue
                        break
                    case LAError.userCancel?:
                        message = ErrorMessages.userCancel.rawValue
                        break
                    case LAError.userFallback?:
                        message = ErrorMessages.userFallback.rawValue
                        break
                    case LAError.biometryNotAvailable?:
                        message = ErrorMessages.biometryNotAvailable.rawValue.replacingOccurrences(of: "BIOMETRIC", with: self.biometricsMethod)
                        break
                    case LAError.biometryNotEnrolled?:
                        message = ErrorMessages.biometryNotEnrolled.rawValue.replacingOccurrences(of: "BIOMETRIC", with: self.biometricsMethod)
                        break
                    case LAError.biometryLockout?:
                        message = ErrorMessages.biometryLockout.rawValue.replacingOccurrences(of: "BIOMETRIC", with: self.biometricsMethod)
                        break
                    default:
                        message = ErrorMessages.biometryNotEnrolled.rawValue.replacingOccurrences(of: "BIOMETRIC", with: self.biometricsMethod)
                        break
                    }
                }
                else
                {
                    switch evaluateError
                    {
                    case LAError.authenticationFailed?:
                        message = ErrorMessages.authenticationFailed.rawValue.replacingOccurrences(of: "BIOMETRIC", with: self.biometricsMethod)
                        break
                    case LAError.userCancel?:
                        message = ErrorMessages.userCancel.rawValue.replacingOccurrences(of: "BIOMETRIC", with: self.biometricsMethod)
                        break
                    case LAError.userFallback?:
                        message = ErrorMessages.userFallback.rawValue.replacingOccurrences(of: "BIOMETRIC", with: self.biometricsMethod)
                        break
                    default:
                        message = ErrorMessages.biometryNotEnrolled.rawValue.replacingOccurrences(of: "BIOMETRIC", with: self.biometricsMethod)
                        break
                    }
                }
                completion(message)
                
            }
        }
    }
    
    func askBiometricsPermission(in view: UIViewController,completion: @escaping()->Void)
    {
        let alertText : String = "¿Quieres guardar tus datos para iniciar sesión con \(biometricsMethod)?"
        AlertsPresenter.showAlertWithAction(alertTitle: "Biometrics", alertMessage: alertText, actionTitle: NSLocalizedString(ButtonsTitle.allow.rawValue, comment: "nil"), actionStyle: .default, inView: view, completion: completion)
    }
}
