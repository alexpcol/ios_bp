//
//  ActivityPresenter.swift
//  boilerPlateAlex
//
//  Created by Mario on 9/21/18.
//  Copyright © 2018 Mario. All rights reserved.
//

import UIKit

class ActivityPresenter: NSObject {
    
    private override init() {}
    
    static func showActivityViewController(withActivityItems activityItems: [Any], andExcludedActivities excludedActivities: [UIActivity.ActivityType], inView view: UIViewController)
    {
        let activityController = UIActivityViewController(activityItems: activityItems, applicationActivities: nil)
        activityController.excludedActivityTypes = excludedActivities
        view.present(activityController, animated: true, completion: nil)
    }
    
    //MARK:- Media Activities
    static func showImagePickerFromCamera(view: UIImagePickerControllerDelegate & UINavigationControllerDelegate)
    {
        guard let viewController = view as? UIViewController else { return }
        let myPickerController = UIImagePickerController()
        myPickerController.delegate = view
        myPickerController.sourceType = UIImagePickerController.SourceType.camera
        viewController.present(myPickerController, animated: true, completion: nil)
    }
    
    static func showImagePickerFromGallery(view: UIImagePickerControllerDelegate & UINavigationControllerDelegate)
    {
        guard let viewController = view as? UIViewController else { return }
        let myPickerController = UIImagePickerController()
        myPickerController.delegate = view
        myPickerController.sourceType = UIImagePickerController.SourceType.photoLibrary
        viewController.present(myPickerController, animated: true, completion: nil)
    }

}
