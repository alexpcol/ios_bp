//
//  AlertsPresenter.swift
//  boilerPlateAlex
//
//  Created by Mario on 9/18/18.
//  Copyright © 2018 Mario. All rights reserved.
//

import UIKit

class AlertsPresenter: NSObject
{
    private override init() {}
    
    static func showOKAlert(title:String?, message:String?, view:UIViewController)
    {
        let okAction = UIAlertAction(title: "OK", style: .default) { (alert) in }
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(okAction)
        view.present(alertController, animated: true, completion: nil)
    }
    
    static func showAlertWithAction(alertTitle: String?, alertMessage: String?, actionTitle: String?, actionStyle: UIAlertAction.Style ,inView view:UIViewController, completion: @escaping()-> Void)
    {
        let alertController = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert)
        
        //For now It could always be cancel... maybe
        let cancelAction = UIAlertAction(title: NSLocalizedString(ButtonsTitle.cancel.rawValue, comment: "nil"), style: .cancel)
        
        alertController.addAction(cancelAction)
        let action = UIAlertAction(title: actionTitle, style: actionStyle) { (_) in
            completion()
        }
        alertController.addAction(action)
        alertController.preferredAction = action
        view.present(alertController, animated: true)
    }
    
    
    static func showAlertWithTextField(alertTitle: String?, alertMessage: String?, actionTitle: String?, actionStyle: UIAlertAction.Style, textFieldPlaceHolder: String?, inView view: UIViewController, completion: @escaping(String?)-> Void)
    {
        let alertController = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert)
        //For now It could always be cancel... maybe
        let cancelAction = UIAlertAction(title: NSLocalizedString(ButtonsTitle.cancel.rawValue, comment: "nil"), style: .cancel)
        alertController.addAction(cancelAction)
        let action = UIAlertAction(title: actionTitle, style: actionStyle) { (_) in
            if let text = alertController.textFields?.first?.text {
                completion(text)
            }
        }
        action.isEnabled = false
         alertController.addAction(action)
        
        alertController.addTextField { (textField) in
            textField.placeholder = textFieldPlaceHolder
            NotificationCenter.default.addObserver(forName: UITextField.textDidChangeNotification, object: textField, queue: OperationQueue.main, using:
                {_ in
                    if let value = textField.text?.isEmpty
                    {
                        action.isEnabled = !value
                    }
            })
        }
        alertController.preferredAction = action
        view.present(alertController, animated: true)
    }
    
    static func showActionSheet(actions:[UIAlertAction], title:String?, message:String?, inView view:UIViewController)
    {
        let cancelAction = UIAlertAction(title: NSLocalizedString(ButtonsTitle.cancel.rawValue, comment: "nil"), style: .cancel)
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
        for action in actions
        {
            alertController.addAction(action)
        }
        alertController.addAction(cancelAction)
        view.present(alertController, animated: true, completion: nil)
    }
}
