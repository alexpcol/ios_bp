//
//  Constants.swift
//  boilerPlateAlex
//
//  Created by Mario on 10/8/18.
//  Copyright © 2018 Mario. All rights reserved.
//

import Foundation
enum APIKeys: String {
    case apiKeyV3 = "12345678"
}

enum URLS: String {
    case apiURL = "https://api.com/1"
}

//Here I type the endPoints
enum URLPaths: String {
    case getUsers = "/users"
}

// In case the url goes with fields
enum QueryString: String {
    case apiKey = "?api_key="
    case language = "&language="
}
// Cell id's for CollectionViews or TableView's
enum CellsIdentifiers: String{
    case cellName = "cell"
}

// Names of the XIB Files
enum NibNames: String{
    case viewNib = "Nib"
}
// Keys for the cache items
enum CacheKeys: String{
    case item = "itemKey"
}

// Alerts titles texts
enum MessagesTitle: String{
    case title = "Title"
    case accessToCameraTitle = "\"App\" Would Like to Access the Camera"
    case accessToLocationTitle = "\"App\" Would Like to Access to your location"
    case camera = "Camera"
    case touchId = "Enable Touch ID"
    case faceId = "Enable Face ID"
}

// Alerts messages texts
enum MessagesText: String{
    case message = "Mensaje genérico"
    case accessToCameraMessage = "Generic message for camera permission."
    case accessToLocationMessage = "Generic message for location permission."
    case noCameraInDevice = "Device doesn't have a camera"
    case biometricsPermissionMessage = "Would you like to save your biometrics info to use BIOMETRICMETHOD?"
}


// Button title texts
enum ButtonsTitle :String{
    case openSettings = "Open Settings"
    case notNow = "Not now"
    case allow = "Allow"
    case cancel = "Cancel"
}
// Objects fields
enum ServicesFieldsKeys: String{
    case id = "id"
}
// ViewControllers ID's
enum ViewControllerIdentifiers: String{
    case vc = "ViewController"
}
// Error messages
enum ErrorMessages: String{
    
    //MARK: - Network messages
    case noInternet = "No Internet connection"
    case sorryErrorSystem = "Sorry! an error occurred in the system, please try again later"
    case error404 = "Error 404: Resource not found"
    case error400 = "Error 400: Bad request"
    
    //MARK: - Biometrics messages
    case authenticationFailed = "Something went wrong trying to verify your identity."
    case userCancel = "User cancelled"
    case userFallback = "User pressed password"
    case biometryNotAvailable = "BIOMETRIC is not available."
    case biometryNotEnrolled = "BIOMETRIC is not configured."
    case biometryLockout = "BIOMETRIC is blocked."
}

// User defualts keys
enum UserDefaultsKeys: String{
    case biometricsLogin = "biometricsLogin"
}


// NotificationCenter UserInfo Keys

enum NotificationCenterUserInfoKeys: String{
    case showCameraAgain = "showCameraAgain"
    case locationUpdated = "locationUpdated"
}
