//
//  NotificationsNames.swift
//  boilerPlateAlex
//
//  Created by Mario on 10/10/18.
//  Copyright © 2018 Mario. All rights reserved.
//
//You should have it in separate files like this?
import Foundation


extension Notification.Name{
    static let showCameraOptionsAgain = Notification.Name("notificationCenter.showCameraOptionsAgain")
    static let locationUpdated = Notification.Name("notificationCenter.locationUpdated")
}
