//
//  TableViewController.swift
//  boilerPlateAlex
//
//  Created by Mario on 10/18/18.
//  Copyright © 2018 Mario. All rights reserved.
//

import UIKit
import CoreData

class TableViewController: UIViewController {

    // MARK: - Variables
    var arrayUser = [User]()
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        loadItems()
    }
    
    // MARK: - Action Methods
    @IBAction func addItem(_ sender: Any) {
        AlertsPresenter.showAlertWithTextField(alertTitle: nil, alertMessage: "New Item", actionTitle: "Add", actionStyle: .default, textFieldPlaceHolder: "Name your item...", inView: self) { (text) in
            //print(text)
            let newUser : User = User(context: self.context)
            newUser.name = text
            newUser.email = text
            newUser.dateCreated = Date()
            self.insert(item: newUser)
        }
    }
    // MARK: - General Methods
    func insert(item: User)
    {
        arrayUser.insert(item, at: 0)
        let indexPath = IndexPath(row: 0, section: 0)
        tableView.insertRows(at: [indexPath], with: .automatic)
        self.saveItems()
    }
    func edit(item: User, indexPath: IndexPath)
    {
        AlertsPresenter.showAlertWithTextField(alertTitle: nil, alertMessage: "Edit Item", actionTitle: "Edit", actionStyle: .default, textFieldPlaceHolder: item.name, inView: self) { (text) in
            self.arrayUser[indexPath.row].name = text
            self.saveItems()
            self.tableView.reloadData()
        }
    }
    
    func delete(indexPath: IndexPath, tableView: UITableView)
    {
        context.delete(arrayUser[indexPath.row])
        arrayUser.remove(at: indexPath.row)
        saveItems()
        tableView.deleteRows(at: [indexPath], with: .automatic)
    }
    
    // MARK: - CRUD Methods
    func saveItems()
    {
        do{
            try context.save()
        }catch{
            print("Error saving context: \(error)")
        }
    }
    
    func loadItems()
    {
        let request: NSFetchRequest<User> = User.fetchRequest()
        let sort = NSSortDescriptor(key: #keyPath(User.dateCreated), ascending: false)
        request.sortDescriptors = [sort]
        do{
           arrayUser = try context.fetch(request)
        }catch{
            print("Error fetching data from context \(error)")
        }
        
    }
    
}

extension TableViewController: UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayUser.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        cell.textLabel?.text = arrayUser[indexPath.row].name
        
        return cell
    }
    
}

extension TableViewController: UITableViewDelegate
{
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let editRow = UITableViewRowAction(style: .normal, title: "Edit") { (rowAction, indexPath) in
            self.edit(item: self.arrayUser[indexPath.row], indexPath: indexPath)
        }
        editRow.backgroundColor = UIColor.blue
        let deleteRow = UITableViewRowAction(style: .destructive, title: "Delete") { (rowAction, indexPath) in
            self.delete(indexPath: indexPath, tableView: tableView)
        }
        return[deleteRow, editRow]
    }
}
