//
//  MapViewController.swift
//  boilerPlateAlex
//
//  Created by Mario on 10/17/18.
//  Copyright © 2018 Mario. All rights reserved.
//

import UIKit
import MapKit
class MapViewController: UIViewController {

    // MARK: - Variables
    @IBOutlet weak var mapView: MKMapView!
    // MARK: - Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(handleLocation(_:)), name: .locationUpdated, object: nil)
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress(_:)))
        longPress.minimumPressDuration = 1
        
        mapView.addGestureRecognizer(longPress)
    }
    override func viewDidAppear(_ animated: Bool) {
        LocationHelper.shared.authorize(in: self)
    }
    // MARK: - General Methods
    @objc func handleLocation(_ notification: Notification)
    {
        if let location = notification.userInfo?[NotificationCenterUserInfoKeys.locationUpdated.rawValue] as? CLLocation
        {
            mapView.setRegion(MapsHelper.createRegionForMap(latitude: location.coordinate.latitude,
                                                            longitude: location.coordinate.longitude,
                                                            latDelta: 0.01,
                                                            lonDelta: 0.01), animated: false)
        }
    }
    
    @objc func handleLongPress(_ sender: UIGestureRecognizer)
    {
        if sender.state == .began { DeviceHelpers.hapticFeedback() }
        if sender.state == .ended {
            let location = LocationHelper.shared.getLocationFromMapTouch(sender: sender, in: mapView)
            MapsHelper.addPin(latitude: location.latitude, longitude: location.longitude, with: "Pin", and: "Yeah pin", in: mapView)
        }
    }
    
    @IBAction func focusMapOnCurrentLocation(_ sender: Any) {
        if let coordinate = LocationHelper.shared.newLocation {
            mapView.setRegion(MapsHelper.createRegionForMap(latitude: coordinate.coordinate.latitude,
                                                            longitude: coordinate.coordinate.longitude,
                                                            latDelta: 0.01,
                                                            lonDelta: 0.01), animated: true)
        }
    }
    
}
