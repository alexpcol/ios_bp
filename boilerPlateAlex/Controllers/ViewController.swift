//
//  ViewController.swift
//  boilerPlateAlex
//
//  Created by Mario on 9/18/18.
//  Copyright © 2018 Mario. All rights reserved.
//

import UIKit
import LocalAuthentication
class ViewController: UIViewController {

    @IBOutlet weak var centralImageView: CustomImageView!
    
    @IBOutlet weak var containerView: UIView!
    
    let image = "https://m.media-amazon.com/images/M/MV5BZjdkOTU3MDktN2IxOS00OGEyLWFmMjktY2FiMmZkNWIyODZiXkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_SY1000_SX675_AL_.jpg"
    let notImage = "https://www.hackingwithswift.com/example-code/system/how-to-cache-data-using-nscache"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configuire()
        NotificationCenter.default.addObserver(self, selector: #selector(showCameraOptions(_:)), name: .showCameraOptionsAgain, object: nil)
        
    }
    
    func configuire(){
        UIHelper.makeCardViewWithShadow(for: containerView)
        UIHelper.roundCorners(for: centralImageView)
        centralImageView.getImage(withURL: image)
        
        //        AlertsPresenter.showAlertWithAction(alertTitle: "Title", alertMessage: nil, actionTitle: "Cool", actionStyle: .default, inView: self) {
        //            print("LALALALA")
        //        }
        
    }
    
    @objc func showCameraOptions(_ notification: Notification)
    {
        if let value = notification.userInfo?[NotificationCenterUserInfoKeys.showCameraAgain.rawValue] as? Bool
        {
            if value { cameraOptions()}
        }
    }
    
    func cameraOptions()
    {
        let camera = UIAlertAction(title: "Camera", style: .default) { (alertAction) in
            ActivityPresenter.showImagePickerFromCamera(view: self)
        }
        
        let library = UIAlertAction(title: "Library", style: .default) { (alertAction) in
            ActivityPresenter.showImagePickerFromGallery(view: self)
        }
    
        AlertsPresenter.showActionSheet(actions: [camera, library], title: "Hola", message: nil, inView: self)
    }
    
    
    
    @IBAction func firstAction(_ sender: Any)
    {
        //1) Test Camera and Gallery permissions
        //if DeviceHelpers.prepareCamera(view: self) { cameraOptions() }
        
        
        //2) Show second View
        //        let view: UIViewController = (self.storyboard?.instantiateViewController(withIdentifier: "SecondViewController"))!
        //        self.navigationController?.pushViewController(view, animated: true)
        
        
        //3) Test Biometrics Auth
        if BiometricIDAuth.shared.canEvaluatePolicy()
        {
            BiometricIDAuth.shared.askBiometricsPermission(in: self) {
                do {
                    let passwordItem = KeychainPasswordItem(service: KeychainConfiguration.serviceName,
                                                            account: "Alex",
                                                            accessGroup: KeychainConfiguration.accessGroup)

                    // Save the password for the new item.
                    try passwordItem.savePassword("123456")
                } catch {
                    fatalError("Error updating keychain - \(error)")
                }
            }
        }
        else { AlertsPresenter.showOKAlert(title: "Error", message: "No biometric HW found", view: self) }
        
    }
    
    
    @IBAction func secondAction(_ sender: Any)
    {
        if BiometricIDAuth.shared.canEvaluatePolicy()
        {
            touchIDAction()
        }
    }
    
    func getData()
    {
        do {
            let passwordItem = KeychainPasswordItem(service: KeychainConfiguration.serviceName,
                                                    account: "Alex",
                                                    accessGroup: KeychainConfiguration.accessGroup)
            let keychainPassword = try passwordItem.readPassword()
            
            print(keychainPassword)
        }
        catch {
            fatalError("Error reading password from keychain - \(error)")
        }
    }
    func touchIDAction()
    {
        BiometricIDAuth.shared.authenticateUser { [weak self] message in
            if let message = message
            {
                DispatchQueue.main.async {
                    AlertsPresenter.showOKAlert(title: "Error", message: message, view: self!)
                }
            }
            else
            {
                self?.getData()
            }
        }
    }
    
    func codableExample(){
        //let decoder = JSONDecoder()
        //let message = try decoder.decode(Message.self, from: json)
        //let imageO = try decoder.decode(ImageAttachment.self, from: json2)
        //
        //print("\(message.from) says: '\(message.text)'")
        //print("\(imageO.url)")
        //
        //for attachment in message.attachments {
        //    switch attachment {
        //    case .image(let image):
        //        print("found 'image' at \(image.url) with size \(image.width)x\(image.height)")
        //    case .audio(let audio):
        //        print("found 'audio' titled \"\(audio.title)\"")
        //    case .unsupported:
        //        print("unsupported")
        //    }
        //}
        
        
        
        Attachment2.register(ImageAttachment.self, for: "image")
        Attachment2.register(AudioAttachment.self, for: "audio")
        Attachment2.register(PdfAttachment.self, for: "pdf")
        do{
            let decoder = JSONDecoder()
            let message = try decoder.decode(Message.self, from: json)
            
            for attachment in message.attachments{
                switch attachment.payload {
                case let image as ImageAttachment:
                    print("found 'image' at \(image.url) with size \(image.width)x\(image.height)")
                    break
                case let audio as AudioAttachment:
                    print("found 'audio' titled \"\(audio.title)\"")
                    break
                case let pdf as PdfAttachment:
                    print("PDF title: \(pdf.title) summary: \(pdf.summary)")
                    break
                default:
                    break
                }
            }
        }catch{
            
        }
    }
    
}

extension ViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        centralImageView.image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        self.dismiss(animated: true, completion: nil)
    }
}

