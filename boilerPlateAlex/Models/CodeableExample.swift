//
//  CodeableExample.swift
//  boilerPlateAlex
//
//  Created by Chila on 1/18/19.
//  Copyright © 2019 Mario. All rights reserved.
//

import Foundation

struct ImageAttachment: Codable {
    let url: URL
    let width: Int
    let height: Int
}

struct AudioAttachment: Codable {
    let title: String
    let url: URL
    let shouldAutoplay: Bool
}

struct PdfAttachment: Codable{
    let title: String
    let summary: String
    let hasEbook: Bool
}

enum Attachment {
    case image(ImageAttachment)
    case audio(AudioAttachment)
    case unsupported
}

extension Attachment: Codable {
    private enum CodingKeys: String, CodingKey {
        case type
        case payload
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let type = try container.decode(String.self, forKey: .type)
        
        switch type {
        case "image":
            let payload = try container.decode(ImageAttachment.self, forKey: .payload)
            self = .image(payload)
        case "audio":
            let payload = try container.decode(AudioAttachment.self, forKey: .payload)
            self = .audio(payload)
        default:
            self = .unsupported
        }
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        switch self {
        case .image(let attachment):
            try container.encode("image", forKey: .type)
            try container.encode(attachment, forKey: .payload)
        case .audio(let attachment):
            try container.encode("audio", forKey: .type)
            try container.encode(attachment, forKey: .payload)
        case .unsupported:
            let context = EncodingError.Context(codingPath: [], debugDescription: "Invalid attachment.")
            throw EncodingError.invalidValue(self, context)
        }
    }
}

struct Attachment2: Codable{
    let type: String
    let payload: Any?
    
    private typealias AttachmentDecoder = (KeyedDecodingContainer<CodingKeys>) throws -> Any
    private typealias AttachmentEncoder = (Any, inout KeyedEncodingContainer<CodingKeys>) throws -> Void
    
    private static var decoders: [String: AttachmentDecoder] = [:]
    private static var encoders: [String: AttachmentEncoder] = [:]
    
    private enum CodingKeys: String, CodingKey{
        case type
        case payload
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        type = try container.decode(String.self, forKey: .type)
        
        if let decode = Attachment2.decoders[type] {
            payload = try decode(container)
        } else {
            payload = nil
        }
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(type, forKey: .type)
        if let payload = self.payload {
            guard let encode = Attachment2.encoders[type] else {
                let context = EncodingError.Context(codingPath: [], debugDescription: "Invalid attachment: \(type).")
                throw EncodingError.invalidValue(self, context)
            }
            try encode(payload, &container)
        } else {
            try container.encodeNil(forKey: .payload)
        }
    }
    
    static func register<A: Codable>(_ type: A.Type, for typeName: String) {
        decoders[typeName] = { container in
            try container.decode(A.self, forKey: .payload)
        }
        encoders[typeName] = { payload, container in
            try container.encode(payload as! A, forKey: .payload)
        }
    }
}

struct Message: Codable {
    let from: String
    let text: String
    let attachments: [Attachment2]
}

let json = """
{
    "from": "Guille",
    "text": "Look what I just found!",
    "attachments": [
        {
            "type": "image",
            "payload": {
                "url": "http://via.placeholder.com/640x480",
                "width": 640,
                "height": 480
            }
        },
        {
            "type": "audio",
            "payload": {
                "title": "Never Gonna Give You Up",
                "url": "https://contoso.com/media/NeverGonnaGiveYouUp.mp3",
                "shouldAutoplay": true,
            }
        },
        {
            "type": "pdf",
            "payload": {
                "title": "The calculating stars",
                "summary": "A lady physist who is an austronaut trying to save the earth",
                "hasEbook": true,
            }
        }
    ]
}
""".data(using: .utf8)!

let json2 = """
{
    "url": "http://via.placeholder.com/640x480",
    "width": 640,
    "height": 480
}
""".data(using: .utf8)!











