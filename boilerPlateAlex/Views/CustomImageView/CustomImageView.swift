//
//  CustomImageView.swift
//  boilerPlateAlex
//
//  Created by Mario on 10/8/18.
//  Copyright © 2018 Mario. All rights reserved.
//

import UIKit

class CustomImageView: UIImageView {

    static let cache = NSCache<NSString,ImageCacheDiscardable>()
    var shouldShowDefault = true
    
    var urlStringItem: String?
    var emptyImage: UIImage?
    
    func getImage(withURL urlString:String, completion: (() -> ())? = nil){
        image = nil
        self.urlStringItem = urlString
        if let imageCache = CustomImageView.cache.object(forKey: urlString as NSString)
        {
            shouldShowDefault = false
            DispatchQueue.main.async {
                self.image = imageCache.image
                //This one can improve
               /* UIView.transition(with: self,
                                  duration:0.45,
                                  options: .transitionCrossDissolve,
                                  animations: {self.image = imageCache.image },
                                  completion: nil)*/
            }
            completion?()
        }
        else
        {
            guard let url = URL(string: urlString) else{
                if self.shouldShowDefault{ image = emptyImage}
                return
            }
            downloadImage(withURL: url, completion: completion)
        }
    }
    
    func downloadImage(withURL url:URL, completion: (() -> ())? = nil)
    {
        let dataTask = URLSession.shared.dataTask(with: url) { data, responseURL, error in
            if error != nil{
                return
            }
            DispatchQueue.main.async {
                if let image = UIImage(data: data!)
                {
                    let imageCache = ImageCacheDiscardable(image: image)
                    CustomImageView.cache.setObject(imageCache, forKey: url.absoluteString as NSString)
                    if url.absoluteString == self.urlStringItem
                    {
                        self.shouldShowDefault = false
                        self.image = image
                        //This one can improve
                        /*DispatchQueue.main.async {
                            UIView.transition(with: self,
                                              duration:0.45,
                                              options: .transitionCrossDissolve,
                                              animations: {self.image = image },
                                              completion: nil)
                        }*/
                        completion?()
                    }
                }
            }
            
        }
        dataTask.resume()
    }

}
